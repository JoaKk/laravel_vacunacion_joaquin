<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $fillable = ['vacunado','fechaVacuna'];
    use HasFactory;

    public function getRouteKeyName()
    {
        return 'slug';
    }   

    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }
}

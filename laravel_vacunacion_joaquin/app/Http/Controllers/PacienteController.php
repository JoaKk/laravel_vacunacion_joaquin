<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\Vacuna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class PacienteController extends Controller
{
    public function vacunar(Paciente $paciente,Vacuna $vacuna){
        $paciente->update([
            'vacunado' => true,
            'fechaVacuna' => Date::now()
        ]);
        return back()->with("mensaje", "El paciente ".$paciente->nombre." se ha vacunado.");
    }

    public function buscador(){
        return view('pacientes.buscador');
    }

    public function buscar(Request $request){
        $busqueda = $request->busqueda;
        $pacientes = Paciente::select('nombre')
        ->where('nombre','LIKE',"%$busqueda%")
        ->pluck('nombre');
        return response()->json($pacientes);
    }
}

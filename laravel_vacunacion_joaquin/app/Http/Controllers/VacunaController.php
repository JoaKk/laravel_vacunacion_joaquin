<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\Vacuna;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VacunaController extends Controller
{
    public function index()
    {
        $vacunas = Vacuna::all();
        return view('vacunas.index', compact('vacunas'));
    }

    public function show(Vacuna $vacuna)
    {
        return view('vacunas.show', compact('vacuna'));
    }

    public function crear(Request $request)
    {
        $vacuna = new Vacuna();
        $vacuna->nombre = $request->nombre;
        $vacuna->slug = Str::slug($request->nombre);
        $vacuna->save();
        return response()->json(['mensaje' => "Vacuna" . $request->nombre . " insertada correctamente."]);
    }

    public function mostrarVacunas($id){
        $paciente = Paciente::find($id);
        $grupo = $paciente->grupo;
        $vacunas = $grupo->vacunas;
        return $vacunas;
    }
}

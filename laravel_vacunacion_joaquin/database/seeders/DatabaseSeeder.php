<?php

namespace Database\Seeders;

use App\Models\Paciente;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(GrupoSeeder::class);
        $this->call(VacunaSeeder::class);
        $pacientes = Paciente::factory(25)->create();      
    }
}

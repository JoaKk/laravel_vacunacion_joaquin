<?php

namespace Database\Factories;

use App\Models\Grupo;
use App\Models\Paciente;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->name;
        $vacunado = $this->faker->boolean;
        $fechaVacuna = null;
        if($vacunado){
            $fechaVacuna = $this->faker->dateTimeThisYear;
        }
        return [
            'nombre' => $nombre,
            'slug' => Str::slug($nombre),
            'vacunado' => $vacunado,
            'grupo_id' => Grupo::all()->random()->id,
            'fechaVacuna' => $fechaVacuna,
        ];
    }
}

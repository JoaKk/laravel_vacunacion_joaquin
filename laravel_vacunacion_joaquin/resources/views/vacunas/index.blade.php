@extends('layouts.master')
@section('titulo')
    Vacunas
@endsection
@section('contenido')
    <div class="row">
        @foreach ($vacunas as $vacuna)
            <div class="col-xs-12 col-sm-6 col-md-4 ">
                <div class="card">
                    <div class="card-header">
                        {{ $vacuna->nombre }}
                    </div>
                    <div class="card-body">
                        <p>Posibles grupos de vacunación</p>
                        <ul>
                            @foreach ($vacuna->grupos as $grupo)
                                <li>{{$grupo->nombre}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/estilo.css') }}">
@endsection

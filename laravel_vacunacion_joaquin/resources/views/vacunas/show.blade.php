@extends('layouts.master')
@section('titulo')
    Vacunas
@endsection
@section('contenido')
    @php
    if (isset($mensaje)) {
        echo "<div style='background-color: greenyellow;height:50px;''>$mensaje";
    }
    @endphp
    @if (session('mensaje'))
    <div class="bg-warning">{{session('mensaje')}}</div>
    @endif
    <h2>{{ $vacuna->nombre }}</h2>
    <h2>Pacientes NO VACUNADOS:</h2>
    <table class="table">
        <thead>
            <th>Nombre</th>
            <th>Grupo de vacunación</th>
            <th>Prioridad</th>
            <th>Acción</th>
        </thead>
        <tbody>
            @foreach ($vacuna
            ->grupos()
            ->orderByDesc('prioridad')
            ->get()
        as $grupo)
                @foreach ($grupo->pacientes as $paciente)
                    @if (!$paciente->vacunado)
                        <tr>
                            <th>{{ $paciente->nombre }}</th>
                            <th>{{ $paciente->grupo->nombre }}</th>
                            <th>{{ $grupo->prioridad }}</th>
                            <th><a href="{{ route('paciente.vacunar', $paciente) }}" class="btn btn-primary"
                                    role="button">Vacunar</a></th>
                        </tr>
                    @endif
                @endforeach
            @endforeach
        </tbody>
    </table>
    <h2>Pacientes vacunados</h2>
    <div class="row">
        @foreach ($vacuna
            ->grupos()
            ->orderByDesc('prioridad')
            ->get()
        as $grupo)
            <div class="col-xs-12 col-sm-6 col-md-4 ">
                <div class="card">
                    <div class="card-header">
                        {{ $grupo->nombre }}
                    </div>
                    <div class="card-body">
                        <ul>
                            @foreach ($grupo->pacientes as $paciente)
                                @if ($paciente->vacunado)
                                    <li>{{ $paciente->nombre }} ({{ $paciente->fechaVacuna }})</li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/estilo.css') }}">
@endsection

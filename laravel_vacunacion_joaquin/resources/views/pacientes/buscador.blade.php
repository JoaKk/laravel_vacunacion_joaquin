@extends('layouts.master')
@section('titulo')
    Vacunas
@endsection
@section('contenido')
    <form class="d-flex">
        <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Introduca el nombre del paciente"
            aria-label="Buscar">
    </form>
    <script>
        $(document).ready(function() {
            $("#busqueda").autocomplete({

                source: function(query, result) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('pacientes/busquedaAjax') }}",
                        dataType: "json",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "busqueda": query["term"],
                        },
                        success: function(data) {
                            result(data);
                        }
                    });
                },
            });
        });

    </script>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/estilo.css') }}">
@endsection
@section('js')
   
@endsection
